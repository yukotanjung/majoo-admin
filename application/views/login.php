<!doctype html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <title>Login - Pengguna Majoo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url() . 'assets/images/favicon.ico'; ?>">

    <!-- Bootstrap Css -->
    <link href="<?php echo base_url() . 'assets/css/bootstrap.min.css'; ?>" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="<?php echo base_url() . 'assets/css/icons.min.css'; ?>" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="<?php echo base_url() . 'assets/css/app.min.css'; ?>" id="app-style" rel="stylesheet" type="text/css" />

    <!-- Sweet Alert-->
    <link href="<?php echo base_url() ?>assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />

    <!-- Loader -->
    <link href="<?php echo base_url() ?>assets/css/overlay-loading.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/custom.css" rel="stylesheet">
</head>

<body>
    <!-- Loader -->
    <div class="loading d-none"></div>

    <div>
  <div class="container-fluid p-0">
    <div class="row g-0">
      <div class="col-xl-7">
        <div class="auth-full-bg pt-lg-5 p-4">
          <div class="w-100">
            <div class="bg-overlay"></div>
            <div class="d-flex h-100 flex-column">
              <div class="p-4 mt-auto">
                <div class="row justify-content-center">
                  <div class="col-lg-7">

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end col -->
      <div class="col-xl-5">
        <div class="auth-full-page-content p-md-5 p-4">
          <div class="w-100">
            <div class="d-flex flex-column h-100">
              <div class="mb-4 mb-md-5">
                  <a href="#" class="d-block auth-logo">
                    <img src="assets/images/main-logo.png" alt="" height="40" class="auth-logo-dark">
                  </a>
              </div>
              <div class="my-auto">
                <div>
                  <h5 class="text-primary text-center">Selamat datang</h5>
                  <p class="text-muted text-center">Masuk untuk mulai menggunakan Majoo.</p>
                </div>
                <div class="mt-4">
                  <form id="form-login">
                    <div class="mb-3">
                      <label for="username" class="form-label abu">Email</label>
                      <input value="" type="text" name="email" class="form-control" id="email" placeholder="Masukkan email">
                    </div>
                    <div class="mb-3">
                      <div class="float-end">
                        <a href="#" class="text-muted">Lupa kata sandi?</a>
                      </div>
                      <label for="password" class="form-label abu">Kata sandi</label>
                      <div class="input-group auth-pass-inputgroup">
                        <input id="password" value="" name="password" type="password" class="form-control" placeholder="Masukkan kata sandi" aria-label="Password" aria-describedby="password-addon">
                        <button class="btn btn-light " type="button" id="password-addon">
                          <i class="mdi mdi-eye-outline"></i>
                        </button>
                      </div>
                      <span id="err-password"></span>
                    </div>
                    <div class="mt-3 d-grid">
                      <button class="btn btn-primary waves-effect waves-light" type="submit">Masuk</button>
                    </div>
                  </form>
                  <div class="mt-4 text-center">
                    <h5 class="font-size-14 mb-3">Masuk menggunakan</h5>
                    <ul class="list-inline">
                      <li class="list-inline-item">
                        <a href="javascript::void()" class="social-list-item bg-primary text-white border-primary">
                          <i class="mdi mdi-facebook"></i>
                        </a>
                      </li>
                      <li class="list-inline-item">
                        <a href="javascript::void()" class="social-list-item bg-danger text-white border-danger">
                          <i class="mdi mdi-google"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div class="mt-5 text-center">
                    <p>Belum memiliki akun? <a href="#" class="fw-medium text-primary"> Daftar gratis sekarang</a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end col -->
    </div>
    <!-- end row -->
  </div>
  <!-- end container-fluid -->
</div>

    <!-- end account-pages -->

    <!-- JAVASCRIPT -->
    <script src="<?php echo base_url() . 'assets/libs/jquery/jquery.min.js'; ?>"></script>
    <script src="<?php echo base_url() . 'assets/libs/bootstrap/js/bootstrap.bundle.min.js'; ?>"></script>
    <script src="<?php echo base_url() . 'assets/libs/metismenu/metisMenu.min.js'; ?>"></script>
    <script src="<?php echo base_url() . 'assets/libs/simplebar/simplebar.min.js'; ?>"></script>
    <script src="<?php echo base_url() . 'assets/libs/node-waves/waves.min.js'; ?>"></script>

    <!-- Sweet Alerts js -->
    <script src="<?php echo base_url() ?>assets/libs/sweetalert2/sweetalert2.min.js"></script>

    <!-- App js -->
    <script src="<?php echo base_url() . 'assets/js/app.js'; ?>"></script>
    <script src="<?php echo base_url() . 'assets/js/pages/js-validate.min.js'; ?>"></script>

    <script>
        var hostname = '<?php echo base_url(); ?>';

        $(document).ready(function() {
          $("#form-login").validate({
              rules: {
                  email: {
                      required: true,
                      email : true,
                  },
                  password: {
                      required: true,
                  }
              },
              messages: {
                  email: {
                    required : "Email harus diisi.",
                    email : 'Email yang kamu masukkan tidak valid',
                  },
                  password: "Kata sandi harus diisi.",
              },
              errorElement: 'small',
              errorPlacement: function(error, element) {
                  var placement = $(element).data('error');
                  if (placement) {
                      $(placement).append(error)
                  } else {
                      if (element.attr("name") == "password") {
                          error.insertAfter("#err-password");
                      } else {
                          error.insertAfter(element);
                      }
                  }
              },
              highlight: function(element) {
                  $(element).parent().addClass("text-danger");
              },
              submitHandler: function(form, event) {
                event.preventDefault();
                  var formData = new FormData(form);

                  $.ajax({
                      type: "POST",
                      url: hostname + 'login/submit',
                      data: formData,
                      contentType: false,
                      cache: false,
                      processData: false,
                      beforeSend: function(data) {
                          $('.loading').removeClass('d-none');
                      },
                      success: function(data) {
                          var result = JSON.parse(data);
                          if (result.reason == 'blocked') {
                              Swal.fire(
                                  '',
                                  'Akun anda telah diblokir<br>Silakan hubungi dministrator',
                                  'error'
                              );
                          } else if (result.reason == false) {
                              Swal.fire(
                                  '',
                                  'Email / kata sandi yang kamu masukkan salah',
                                  'info'
                              );
                          } else {
                              window.location.href = hostname + '/produk';
                          }
                      },
                      complete: function(date) {
                          $('.loading').addClass('d-none');
                      }
                  });
              },
          });
        });
    </script>
</body>

</html>
