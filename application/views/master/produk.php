<div class="page-content">
	<div class="container-fluid">

		<!-- start page title -->
		<div class="row">
			<div class="col-12">
				<div class="page-title-box d-sm-flex align-items-center justify-content-between">
					<h4 class="mb-sm-0 font-size-18">Produk</h4>

					<div class="page-title-right">
						<ol class="breadcrumb m-0">
							<li class="breadcrumb-item">Data Master</li>
							<li class="breadcrumb-item active">Produk</li>
						</ol>
					</div>

				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<div class="row mb-2">
							<div class="col-sm-4">
								<div class="search-box me-2 mb-2 d-inline-block">
									<div class="position-relative">
										<input type="text" id="search_produk" class="form-control" placeholder="Cari">
										<i class="bx bx-search-alt search-icon"></i>
									</div>
								</div>
							</div>
							<div class="col-sm-8">
								<div class="text-sm-end">
									<button type="button" id="btn-add-pemanfaat" class="btn btn-success btn-rounded waves-effect waves-light mb-2 me-2" data-bs-toggle="modal" data-bs-target="#myModalProduk"><i class="mdi mdi-plus me-1"></i> Tambah Produk</button>
								</div>
							</div><!-- end col-->
						</div>

						<div class="table-responsive">
							<table id="master_produk" class="table align-middle table-nowrap table-check">
								<thead class="table-light">
									<tr>
										<th class="align-middle">Nama Produk</th>
										<th class="align-middle">Deskripsi</th>
										<th class="align-middle">Harga</th>
										<th class="align-middle">Kategori</th>
										<th class="align-middle">Foto</th>
										<th class="align-middle text-center">Aksi</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- end row -->


	</div> <!-- container-fluid -->
</div>
<!-- End Page-content -->

<!-- Modal Add Produk -->
<div id="myModalProduk" class="modal fade" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="myModalLabel">Tambah Produk</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form id="form_produk" method="post">
				<div class="modal-body">
					<div class="col-lg-12 mb-2">

						<label class="form-label abu">Nama Produk</label>
						<input class="form-control" type="text" name="product_name" id="product_name">
					</div>
					<div class="col-lg-12 mb-2">
						<label class="form-label abu">Kategori</label>
						<select name="category_id" id="category_id" class="form-select select2">
							<option selected value="">Pilih kategori</option>
							<?php foreach ($category as $ls) { ?>
								<option value=" <?php echo $ls['category_id'] ?>">
									<?php echo $ls['category_name'] ?></option>
							<?php } ?>
						</select>
						<small id="err_category"></small>
					</div>
					<div class="col-lg-12 mb-2">
						<label class="form-label abu">Harga</label>
						<input class="form-control" type="text" name="price" id="price">
					</div>
					<div class="col-lg-12 mb-2">
						<label class="form-label abu">Foto</label>
						<input class="form-control" type="file" accept="image/*" name="photo" id="photo">
					</div>
					<div class="col-lg-12 mb-2">
						<label class="form-label abu">Deskripsi</label>

						<textarea id="description" name="description"></textarea>
						<p class="p-0 m-0" id="err_description"></p>
					</div>


				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Batal</button>
					<button type="submit" id="btn-save-pemanfaat" class="btn btn-primary waves-effect waves-light">Simpan</button>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- Modal Update Produk -->
<div id="myModalProdukUpdate" class="modal fade" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="myModalLabel">Tambah Produk</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form id="form_produk_upd" method="post">
				<div class="modal-body">
					<div class="col-lg-12 mb-2">
						<input type="hidden" name="id_produk" id="id_produk" value="">
						<label class="form-label abu">Nama Produk</label>
						<input class="form-control" type="text" name="product_name_upd" id="product_name_upd">
					</div>
					<div class="col-lg-12 mb-2">
						<label class="form-label abu">Kategori</label>
						<select name="category_id_upd" id="category_id_upd" class="form-select select2">
							<option selected value="">Pilih kategori</option>
							<?php foreach ($category as $ls) { ?>
								<option value=" <?php echo $ls['category_id'] ?>">
									<?php echo $ls['category_name'] ?></option>
							<?php } ?>
						</select>
						<small id="err_category_upd"></small>
					</div>
					<div class="col-lg-12 mb-2">
						<label class="form-label abu">Harga</label>
						<input class="form-control" type="text" name="price_upd" id="price_upd">
					</div>
					<div class="col-lg-12 mb-2">
						<label class="form-label abu">Foto</label>
						<input class="form-control" type="file" accept="image/*" name="photo_upd" id="photo_upd">
					</div>
					<div class="col-lg-12 mb-2 text-center">
						<img width="100" id="preview_product" src="" alt="">
					</div>
					<div class="col-lg-12 mb-2">
						<label class="form-label abu">Deskripsi</label>

						<textarea id="description_update" name="description_update"></textarea>
						<p class="p-0 m-0" id="err_description_upd"></p>
					</div>


				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Batal</button>
					<button type="submit" id="btn-save-pemanfaat" class="btn btn-primary waves-effect waves-light">Simpan</button>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
