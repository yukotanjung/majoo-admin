<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_login extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function get_employee($username, $password){
        $password = sha1(md5($password));

        $this->db->from('tbl_login a');
        $this->db->where('email',$username);
        $this->db->where('password',$password);
        $query = $this->db->get();
        $hasil= $query->row_array();

        if($query->num_rows() > 0) {
            if($hasil['flag'] == 1){
              $this->session->set_userdata($hasil);
              return $hasil;
            } else {
                return 'blocked';
            }
        } else {
            return false;
        }
    }
}
