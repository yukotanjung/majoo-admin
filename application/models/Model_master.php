<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_master extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    var $search_produk = array('product_name', 'description','category_name');
    var $order_produk = array('product_name', 'description', 'price', 'category_name');

    var $search_category = array('category_name');
    var $order_category = array('category_name');

    public function _get_data_produk()
  {

    $this->db->select('p.*,tc.category_name,');
    $this->db->join('tbl_category tc', 'tc.category_id = p.category_id', 'left');
    $this->db->from('tbl_produk p');

    $i = 0;

    if (isset($_POST['search']) and !empty($_POST['search'])) {
      foreach ($this->search_produk as $item) {
        if (($_POST['search'])) {
          if ($i === 0) {
            $this->db->group_start();
            $this->db->like($item, $this->security->xss_clean($this->input->post('search')));
          } else {
            $this->db->or_like($item, $this->security->xss_clean($this->input->post('search')));
          }

          if (count($this->search_produk) - 1 == $i) //last loop
            $this->db->group_end(); //close bracket
        }
        $i++;
      }
    }
    if (isset($_POST['order'])) {
      $this->db->order_by($this->order_produk[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else {
      $order = array('p.id' => 'desc');
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  public function records_filter_produk()
  {
    $this->_get_data_produk();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function records_total_produk()
  {

    $this->db->select('*');
    $this->db->from('tbl_produk');
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function get_data_produk()
  {
    $this->_get_data_produk();
    if ($_POST['length'] != -1) {
      $this->db->limit(10, $_POST['start']);
    }
    $query = $this->db->get();
    return $query->result_array();
  }

  public function get_categoryall()
  {
    $data = $this->db->get('tbl_category');
    return $data->result_array();
  }

  public function getdetail($id)
  {
    $this->db->where('id',$id);
    $data = $this->db->get('tbl_produk');
    return $data->row_array();
  }

  public function _get_data_category()
{

  $this->db->select('*');
  $this->db->from('tbl_category');

  $i = 0;

  if (isset($_POST['search']) and !empty($_POST['search'])) {
    foreach ($this->search_category as $item) {
      if (($_POST['search'])) {
        if ($i === 0) {
          $this->db->group_start();
          $this->db->like($item, $this->security->xss_clean($this->input->post('search')));
        } else {
          $this->db->or_like($item, $this->security->xss_clean($this->input->post('search')));
        }

        if (count($this->search_category) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }
  }
  if (isset($_POST['order'])) {
    $this->db->order_by($this->order_category[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
  } else {
    $order = array('category_id' => 'desc');
    $this->db->order_by(key($order), $order[key($order)]);
  }
}

  public function records_filter_category()
  {
    $this->_get_data_category();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function records_total_category()
  {

    $this->db->select('*');
    $this->db->from('tbl_category');
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function get_data_category()
  {
    $this->_get_data_category();
    if ($_POST['length'] != -1) {
      $this->db->limit(10, $_POST['start']);
    }
    $query = $this->db->get();
    return $query->result_array();
  }

  public function getdetailkategori($id)
  {
    $this->db->where('category_id',$id);
    $data = $this->db->get('tbl_category');
    return $data->row_array();
  }


}
