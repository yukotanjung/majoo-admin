<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kategori extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_master', 'master');
    }

    public function index()
    {
      $data['javascript'] = 'kategori.js';
      $this->template->load('template', 'master/kategori', $data);
    }

    public function get_kategori()
    {
      $list = $this->master->get_data_category();

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $ls) {
          $no++;
          $row = array();
          $row[] = '<p class="text-body fw-bold">' . $ls['category_name'] . '</p>';
          $row[] = '<div class="d-flex gap-3">
                          <a onclick="update(\' ' . $ls['category_id'] . ' \')" class="text-success pointer update"><i class="mdi mdi-pencil font-size-18"></i></a>
                          <a onclick="hapus(\' ' . $ls['category_id'] . ' \')" class="text-danger pointer delete"><i class="mdi mdi-trash-can font-size-18"></i></a>
                      </div>';

          $data[] = $row;
        }

        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->master->records_total_category(),
          "recordsFiltered" => $this->master->records_filter_category(),
          "data" => $data,
        );
        echo json_encode($output);
    }

    public function simpan()
    {
      $data = $this->input->post();
      $this->db->insert('tbl_category',$data);
    }

    public function hapus()
    {
      $this->db->delete('tbl_category',array('category_id'=>$this->input->post('id')));
    }

    public function detail()
    {
      $category = $this->master->getdetailkategori($this->input->post('id'));
      echo json_encode($category);
    }

    public function update()
    {
      $data = $this->input->post();

      $upd['category_name'] = $data['category_name_upd'];
      $this->db->update('tbl_category',$upd,array('category_id'=>$data['category_id']));
    }


}
