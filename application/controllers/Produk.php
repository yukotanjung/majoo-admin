<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produk extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_master','master');
    }

    public function index()
    {
      $data['javascript'] = 'produk.js';
      $data['category'] = $this->master->get_categoryall();
      $this->template->load('template', 'master/produk', $data);
    }

    public function get_produk()
    {
      $list = $this->master->get_data_produk();

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $ls) {
          $no++;
          $row = array();
          $row[] = '<p class="text-body fw-bold">' . $ls['product_name'] . '</p>';
          $row[] = $ls['description'];
          $row[] = format_rupiah($ls['price']);
          $row[] = '<p class="text-body fw-bold">' . $ls['category_name'] . '</p>';
          $image = base_url().'photo/'.$ls['photo'];
          $row[] = "<img width='100' src='$image'>";
          $row[] = '<div class="d-flex gap-3">
                          <a onclick="update(\' ' . $ls['id'] . ' \')" class="text-success pointer update"><i class="mdi mdi-pencil font-size-18"></i></a>
                          <a onclick="hapus(\' ' . $ls['id'] . ' \')" class="text-danger pointer delete"><i class="mdi mdi-trash-can font-size-18"></i></a>
                      </div>';

          $data[] = $row;
        }

        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->master->records_total_produk(),
          "recordsFiltered" => $this->master->records_filter_produk(),
          "data" => $data,
        );
        echo json_encode($output);
    }

    public function simpan()
    {

      $ins['product_name'] = $this->input->post('product_name');
      $ins['category_id'] = $this->input->post('category_id');
      $ins['price'] = $this->input->post('price');
      $ins['description'] = $this->input->post('description');

      $config['upload_path']          = FCPATH.'/photo';
      $config['allowed_types']        = 'gif|jpg|png';

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('photo'))
                {

                        echo $this->upload->display_errors();
                }
                else
                {
                        $data = $this->upload->data();

                        $ins['photo'] = $data['file_name'];
                        $this->db->insert('tbl_produk',$ins);
                        echo 1;
                }
    }

    public function hapus()
    {

      $produk = $this->master->getdetail($this->input->post('id'));
      if(unlink(FCPATH.'/photo/'.$produk['photo'])){
        $this->db->delete('tbl_produk',array('id'=>$produk['id']));
      }

    }

    public function detail()
    {
      $produk = $this->master->getdetail($this->input->post('id'));
      $produk['photo'] = base_url().'/photo/'.$produk['photo'];
      echo json_encode($produk);
    }

    public function update()
    {
      $produk = $this->master->getdetail($this->input->post('id_produk'));

      $upd['product_name'] = $this->input->post('product_name_upd');
      $upd['category_id'] = $this->input->post('category_id_upd');
      $upd['price'] = $this->input->post('price_upd');
      $upd['description'] = $this->input->post('description_update');

      if($_FILES['photo_upd']['name'] != "") {
        $config['upload_path']          = FCPATH.'/photo';
        $config['allowed_types']        = 'gif|jpg|png';

                  $this->load->library('upload', $config);

                  if ( ! $this->upload->do_upload('photo_upd'))
                  {

                          echo $this->upload->display_errors();
                  }
                  else
                  {
                          $data = $this->upload->data();

                          $upd['photo'] = $data['file_name'];

                  }
        }

        $this->db->update('tbl_produk',$upd,array('id'=>$produk['id']));
        echo 1;
    }


}
