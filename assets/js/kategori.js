
$(document).ready(function () {
	url = hostname + "kategori/get_kategori";
	table = $("#master_kategori").DataTable({
		processing: true,
		serverSide: true,
		order: [],
		searching: true,
		info: false,
		//"pagingType": "listbox",
		pagingType: "numbers",
		bLengthChange: false,
		bPaginate: true,
		bProcessing: false,
		language: {
			// emptyTable: "<li class='text-danger' align='center'>NUSE nÃ£o encontrada</li>",
			emptyTable: "<p class='text-center'>Data tidak ada</p>",
			zeroRecords: "0 data",
			paginate: {
				previous: "<i class='fa fa-angle-left' aria-hidden='true'></i>",
				next: " <i class='fa fa-angle-right' aria-hidden='true'></i> ",
				first: "<i class='fa fa-angle-double-left' aria-hidden='true'></i> ",
				last: "<i class='fa fa-angle-double-right' aria-hidden='true'></i> ",
			},
		},
		dom: '<"top"l>rt<"bottom left"pi><"caption right"><"clear">',
		ajax: {
			url: url,
			type: "POST",
			data: function (data) {
				data.search = $("#search_kategori").val();
			},
		},
		fnPreDrawCallback: function () {
			$("tbody").html("");
		},
		drawCallback: function (hasil) {
			var api = this.api();
			var records_displayed = api.page.info().recordsDisplay;
		},

		columnDefs: [
			{
				targets: [0],
				orderable: true,
				class: "align-middle",
			},
			{
				targets: [1],
				orderable: false,
				class: "align-middle",
			},
		],

		dom: '<"top"l>rt<"bottom left"pi><"caption right"><"clear">',
	});

	$("#search_kategori").keyup(function(event) {
		table.ajax.reload(null, false);
	});

	$("#form_kategori").validate({

			rules: {
					category_name: {
							required: true,
					},
			},
			messages: {
					category_name: {
						required : "Nama kategori harus diisi.",
					},

			},
			errorElement: 'small',
			errorPlacement: function(error, element) {
					var placement = $(element).data('error');
					if (placement) {
							$(placement).append(error)
					} else {
							error.insertAfter(element);
					}
			},
			highlight: function(element) {
					$(element).parent().addClass("text-danger");
			},
			submitHandler: function(form, event) {
				event.preventDefault();
					var formData = new FormData(form);

					$.ajax({
							type: "POST",
							url: hostname + 'Kategori/simpan',
							data: formData,
							contentType: false,
							cache: false,
							processData: false,
							beforeSend: function(data) {
									$('.loading').removeClass('d-none');
							},
							success: function(data) {
								Swal.fire("Berhasil!", "Berhasil menambahkan data.", "success");

								$(":input", "#form_kategori")
								.not(":button, :submit, :reset, :hidden")
								.val("")
								.prop("checked", false)
								.prop("selected", false);
								$("#myModalKategori").modal('hide');
								table.ajax.reload(null, false);
							},
							complete: function(date) {
									$('.loading').addClass('d-none');
							}
					});
			},
	});

  $("#form_kategori_upd").validate({

			rules: {
					category_name: {
							required: true,
					},
			},
			messages: {
					category_name: {
						required : "Nama kategori harus diisi.",
					},

			},
			errorElement: 'small',
			errorPlacement: function(error, element) {
					var placement = $(element).data('error');
					if (placement) {
							$(placement).append(error)
					} else {
							error.insertAfter(element);
					}
			},
			highlight: function(element) {
					$(element).parent().addClass("text-danger");
			},
			submitHandler: function(form, event) {
				event.preventDefault();
					var formData = new FormData(form);

					$.ajax({
							type: "POST",
							url: hostname + 'kategori/update',
							data: formData,
							contentType: false,
							cache: false,
							processData: false,
							beforeSend: function(data) {
									$('.loading').removeClass('d-none');
							},
							success: function(data) {
								Swal.fire("Berhasil!", "Berhasil mengubah data.", "success");

								$(":input", "#form_kategori_upd")
								.not(":button, :submit, :reset, :hidden")
								.val("")
								.prop("checked", false)
								.prop("selected", false);
								$("#myModalKategoriUpdate").modal('hide');
								table.ajax.reload(null, false);
							},
							complete: function(date) {
									$('.loading').addClass('d-none');
							}
					});
			},
	});




	//$(".table-responsive").responsiveTable();
});

function hapus(id){
	Swal.fire({
						icon: "warning",
						title: "Apakah anda yakin ingin menghapusnya?",
						showCancelButton: true,
						confirmButtonText: "Ya",
						cancelButtonText: "Batal",
					}).then((result) => {
						if (result.isConfirmed) {
							$.ajax({
								type: "POST",
								url:
									hostname + "kategori/hapus/",
								data: {id:id},
								success: function (data) {
                  Swal.fire("Berhasil!", "Berhasil menghapus data.", "success");
									table.ajax.reload(null, false);

								},
							});
						}
					});
}

function update(id) {
	$.ajax({
		type: "POST",
		url:
			hostname + "kategori/detail",
		data: {id:id},
		dataType:"json",
		success: function (data) {
			$("#category_id").val(data.category_id);
      $("#category_name_upd").val(data.category_name);
			$("#myModalKategoriUpdate").modal('show');
		},
	});
}
