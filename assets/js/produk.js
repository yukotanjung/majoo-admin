$("#category_id").select2({
	width: "100%",
	placeholder: "Pilih kategori",
});
$("#category_id_upd").select2({
	width: "100%",
	placeholder: "Pilih kategori",
});
$(document).ready(function () {
	url = hostname + "produk/get_produk";
	table = $("#master_produk").DataTable({
		processing: true,
		serverSide: true,
		order: [],
		searching: true,
		info: false,
		//"pagingType": "listbox",
		pagingType: "numbers",
		bLengthChange: false,
		bPaginate: true,
		bProcessing: false,
		language: {
			// emptyTable: "<li class='text-danger' align='center'>NUSE nÃ£o encontrada</li>",
			emptyTable: "<p class='text-center'>Data tidak ada</p>",
			zeroRecords: "0 data",
			paginate: {
				previous: "<i class='fa fa-angle-left' aria-hidden='true'></i>",
				next: " <i class='fa fa-angle-right' aria-hidden='true'></i> ",
				first: "<i class='fa fa-angle-double-left' aria-hidden='true'></i> ",
				last: "<i class='fa fa-angle-double-right' aria-hidden='true'></i> ",
			},
		},
		dom: '<"top"l>rt<"bottom left"pi><"caption right"><"clear">',
		ajax: {
			url: url,
			type: "POST",
			data: function (data) {
				data.search = $("#search_produk").val();
			},
		},
		fnPreDrawCallback: function () {
			$("tbody").html("");
		},
		drawCallback: function (hasil) {
			var api = this.api();
			var records_displayed = api.page.info().recordsDisplay;
		},

		columnDefs: [
			{
				targets: [0],
				orderable: true,
				class: "align-middle",
			},
			{
				targets: [1],
				orderable: false,
				class: "align-middle",
			},
			{
				targets: [2],
				orderable: true,
				class: "align-middle",
			},
			{
				targets: [3],
				orderable: true,
				class: "align-middle",
			},
			{
				targets: [4],
				orderable: false,
				class: "align-middle",
			},
      {
				targets: [5],
				orderable: false,
				class: "align-middle",
			},
		],

		dom: '<"top"l>rt<"bottom left"pi><"caption right"><"clear">',
	});

	$("#search_produk").keyup(function(event) {
		table.ajax.reload(null, false);
	});

	CKEDITOR.replace( 'description' );
	CKEDITOR.replace( 'description_update' );

	$("#form_produk").validate({
		ignore: [],
			rules: {
					product_name: {
							required: true,
					},
					category_id: {
							required: true,
					},
					price: {
							required: true,
					},
					photo: {
							required: true,
					},
					description: {
						required: true,
					},
			},
			messages: {
					product_name: {
						required : "Nama produk harus diisi.",
					},
					category_id: "Kategori harus diisi.",
					price: "Harga harus diisi.",
					photo: "Foto harus diisi.",
					description: "Deskripsi harus diisi.",

			},
			errorElement: 'small',
			errorPlacement: function(error, element) {
					var placement = $(element).data('error');
					if (placement) {
							$(placement).append(error)
					} else {
							if (element.attr("name") == "category_id") {
									error.insertAfter("#err_category");
							} else if(element.attr("name") == "description"){
								error.insertAfter("#err_description");
							} else {
									error.insertAfter(element);
							}
					}
			},
			highlight: function(element) {
					$(element).parent().addClass("text-danger");
			},
			submitHandler: function(form, event) {
				event.preventDefault();
					var formData = new FormData(form);

					$.ajax({
							type: "POST",
							url: hostname + 'Produk/simpan',
							data: formData,
							contentType: false,
							cache: false,
							processData: false,
							beforeSend: function(data) {
									$('.loading').removeClass('d-none');
							},
							success: function(data) {
								Swal.fire("Berhasil!", "Berhasil menambahkan data.", "success");
								$("#category_id").select2("val", "");
								$("#category_id").val('').trigger('change')
								CKEDITOR.instances.description.setData('');
								$(":input", "#form_produk")
								.not(":button, :submit, :reset, :hidden")
								.val("")
								.prop("checked", false)
								.prop("selected", false);
								$("#myModalProduk").modal('hide');
								table.ajax.reload(null, false);
							},
							complete: function(date) {
									$('.loading').addClass('d-none');
							}
					});
			},
	});

	$("#form_produk_upd").validate({
		ignore: [],
			rules: {
					product_name_upd: {
							required: true,
					},
					category_id_upd: {
							required: true,
					},
					price_upd: {
							required: true,
					},
					description_update: {
						required: true,
					},
			},
			messages: {
					product_name_upd: {
						required : "Nama produk harus diisi.",
					},
					category_id_upd: "Kategori harus diisi.",
					price_upd: "Harga harus diisi.",
					description_update: "Deskripsi harus diisi.",

			},
			errorElement: 'small',
			errorPlacement: function(error, element) {
					var placement = $(element).data('error');
					if (placement) {
							$(placement).append(error)
					} else {
							if (element.attr("name") == "category_id") {
									error.insertAfter("#err_category_upd");
							} else if(element.attr("name") == "description"){
								error.insertAfter("#err_description_upd");
							} else {
									error.insertAfter(element);
							}
					}
			},
			highlight: function(element) {
					$(element).parent().addClass("text-danger");
			},
			submitHandler: function(form, event) {
				event.preventDefault();
					var formData = new FormData(form);

					$.ajax({
							type: "POST",
							url: hostname + 'Produk/update',
							data: formData,
							contentType: false,
							cache: false,
							processData: false,
							beforeSend: function(data) {
									$('.loading').removeClass('d-none');
							},
							success: function(data) {
								Swal.fire("Berhasil!", "Berhasil mengubah data.", "success");
								$("#category_id_upd").select2("val", "");
								$("#category_id_upd").val('').trigger('change')
								CKEDITOR.instances.description_update.setData('');
								$(":input", "#form_produk_upd")
								.not(":button, :submit, :reset, :hidden")
								.val("")
								.prop("checked", false)
								.prop("selected", false);
								$("#myModalProdukUpdate").modal('hide');
								table.ajax.reload(null, false);
							},
							complete: function(date) {
									$('.loading').addClass('d-none');
							}
					});
			},
	});




	//$(".table-responsive").responsiveTable();
});

function hapus(id){
	Swal.fire({
						icon: "warning",
						title: "Apakah anda yakin ingin menghapusnya?",
						showCancelButton: true,
						confirmButtonText: "Ya",
						cancelButtonText: "Batal",
					}).then((result) => {
						if (result.isConfirmed) {
							$.ajax({
								type: "POST",
								url:
									hostname + "produk/hapus/",
								data: {id:id},
								success: function (data) {
									Swal.fire("Berhasil!", "Berhasil menghapus data.", "success");
									table.ajax.reload(null, false);

								},
							});
						}
					});
}

function update(id) {
	$.ajax({
		type: "POST",
		url:
			hostname + "produk/detail",
		data: {id:id},
		dataType:"json",
		success: function (data) {
			$("#id_produk").val(data.id);
			$('#category_id_upd').val(data.category_id);
			$('#category_id_upd').trigger('change');
			$("#product_name_upd").val(data.product_name);
			$("#price_upd").val(data.price);
			$("#preview_product").attr('src', data.photo);
			CKEDITOR.instances.description_update.setData(data.description);
			$("#myModalProdukUpdate").modal('show');

		},
	});
}
