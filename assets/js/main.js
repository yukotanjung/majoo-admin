function company_name(companyName) {
	var company_name = companyName.toLowerCase();
	var check_type = company_name.substr(0, 4);
	switch (check_type) {
		case "pt. ":
			return "PT." + company_name.substr(3).toLowerCase().replace(/(?<= )[^\s]|^./g, a => a.toUpperCase());
			break;
		case "cv. ":
			return "CV." + company_name.substr(3).toLowerCase().replace(/(?<= )[^\s]|^./g, a => a.toUpperCase());
			break;
		case "pg. ":
			return "PG." + company_name.substr(3).toLowerCase().replace(/(?<= )[^\s]|^./g, a => a.toUpperCase());
			break;
		case "pdam":
			return "PDAM" + company_name.substr(4).toLowerCase().replace(/(?<= )[^\s]|^./g, a => a.toUpperCase());
			break;
		case "pdab":
			return "PDAB" + company_name.substr(4).toLowerCase().replace(/(?<= )[^\s]|^./g, a => a.toUpperCase());
			break;
		case "pd. ":
			return "PD." + company_name.substr(3).toLowerCase().replace(/(?<= )[^\s]|^./g, a => a.toUpperCase());
			break;
		case "ud. ":
			return "UD." + company_name.substr(3).toLowerCase().replace(/(?<= )[^\s]|^./g, a => a.toUpperCase());
			break;
		default:
			return companyName.toLowerCase().replace(/(?<= )[^\s]|^./g, a => a.toUpperCase());
	}
}
